from aiogram.dispatcher.filters.state import StatesGroup, State


class ImageStyling(StatesGroup):
    get_content = State()
    get_style = State()
