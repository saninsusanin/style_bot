import os
import copy
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as f
import torchvision.models as models
import torchvision.transforms as transforms


from PIL import Image
from aiogram.types import InputFile


class ContentLoss(nn.Module):

    def __init__(self, target,):
        super(ContentLoss, self).__init__()
        self.target = target.detach()

    def forward(self, input_):
        self.loss = f.mse_loss(input_, self.target)
        return input_


def gram_matrix(input_):
    batch_size, h, w, f_map_num = input_.size()
    features = input_.view(batch_size * h, w * f_map_num)
    gram = torch.mm(features, features.t())

    return gram.div(batch_size * h * w * f_map_num)


class StyleLoss(nn.Module):

    def __init__(self, target_feature):
        super(StyleLoss, self).__init__()
        self.target = gram_matrix(target_feature).detach()

    def forward(self, input_):
        G = gram_matrix(input_)
        self.loss = f.mse_loss(G, self.target)
        return input_


class Normalization(nn.Module):
    def __init__(self, mean, std):
        super(Normalization, self).__init__()
        self.mean = torch.tensor(mean).view(-1, 1, 1)
        self.std = torch.tensor(std).view(-1, 1, 1)

    def forward(self, img):
        # normalize img
        return (img - self.mean) / self.std


class NST:
    def __init__(self, image_size):
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.image_size = image_size
        self.content = self.image_loader(os.environ.get('PATH_TO_CONTENT'))
        self.style = self.image_loader(os.environ.get('PATH_TO_STYLE'))

    def image_loader(self, image_name):
        loader = transforms.Compose([
            transforms.Resize((self.image_size, self.image_size)),
            transforms.ToTensor()])

        image = Image.open(image_name)
        image = loader(image).unsqueeze(0)

        return image.to(self.device, torch.float)

    def get_style_model_and_losses(self, cnn, normalization_mean, normalization_std,
                                   content_layers=('conv_4',),
                                   style_layers=('conv_1', 'conv_2', 'conv_3', 'conv_4', 'conv_5')):
        cnn = copy.deepcopy(cnn)
        normalization = Normalization(normalization_mean, normalization_std).to(self.device)
        content_losses = []
        style_losses = []
        model = nn.Sequential(normalization)

        i = 0
        for layer in cnn.children():
            if isinstance(layer, nn.Conv2d):
                i += 1
                name = 'conv_{}'.format(i)
            elif isinstance(layer, nn.ReLU):
                name = 'relu_{}'.format(i)
                layer = nn.ReLU(inplace=False)
            elif isinstance(layer, nn.MaxPool2d):
                name = 'pool_{}'.format(i)
            elif isinstance(layer, nn.BatchNorm2d):
                name = 'bn_{}'.format(i)
            else:
                raise RuntimeError('Unrecognized layer: {}'.format(layer.__class__.__name__))

            model.add_module(name, layer)

            if name in content_layers:
                target = model(self.content).detach()
                content_loss = ContentLoss(target)
                model.add_module("content_loss_{}".format(i), content_loss)
                content_losses.append(content_loss)

            if name in style_layers:
                target = model(self.style).detach()
                style_loss = StyleLoss(target)
                model.add_module("style_loss_{}".format(i), style_loss)
                style_losses.append(style_loss)

        for i in range(len(model) - 1, -1, -1):
            if isinstance(model[i], ContentLoss) or isinstance(model[i], StyleLoss):
                break

        model = model[:(i + 1)]

        return model, style_losses, content_losses

    @staticmethod
    def get_input_optimizer(input_image):
        optimizer = optim.LBFGS([input_image.requires_grad_()])
        return optimizer

    def run_style_transfer(self,
                           num_steps=3 if not os.environ.get('NUM_OF_ITERATIONS') else
                           os.environ.get('NUM_OF_ITERATIONS'),
                           style_weight=100000, content_weight=1):
        """Run the style transfer."""
        cnn = models.vgg19(pretrained=True).features.to(self.device).eval()
        cnn_normalization_mean = torch.tensor([0.485, 0.456, 0.406]).to(self.device)
        cnn_normalization_std = torch.tensor([0.229, 0.224, 0.225]).to(self.device)
        model, style_losses, content_losses = \
            self.get_style_model_and_losses(cnn, cnn_normalization_mean, cnn_normalization_std)
        input_image = self.content.clone()
        optimizer = NST.get_input_optimizer(input_image)

        run = [0]
        while run[0] <= num_steps:

            def closure():
                input_image.data.clamp_(0, 1)

                optimizer.zero_grad()
                model(input_image)
                style_score = 0
                content_score = 0

                for sl in style_losses:
                    style_score += sl.loss
                for cl in content_losses:
                    content_score += cl.loss

                style_score *= style_weight
                content_score *= content_weight

                loss = style_score + content_score
                loss.backward()

                run[0] += 1
                if run[0] % 50 == 0:
                    print("run {}:".format(run))
                    print('Style Loss : {:4f} Content Loss: {:4f}\n'.format(
                        style_score.item(), content_score.item()))

                return style_score + content_score

            optimizer.step(closure)

        input_image.data.clamp_(0, 1)
        input_image = input_image.squeeze(0)
        input_image = transforms.ToPILImage()(input_image)
        input_image.save('./data/stylized.jpg', 'JPEG')
        input_image = InputFile('./data/stylized.jpg')

        return input_image
