import os
import sys
import logging

from aiogram import Bot, Dispatcher, executor
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.types import BotCommand, Message
from aiogram.dispatcher import FSMContext
from states import ImageStyling
from aiogram.dispatcher.filters import CommandStart, CommandHelp
from networks.nst import NST


API_TOKEN = 'API TOKEN'

if os.environ.get('APITOKEN'):
    API_TOKEN = os.environ.get('APITOKEN')
else:
    sys.stderr.write('No token api has been provided')
    exit(1)

if not os.environ.get('NUM_OF_ITERATIONS'):
    sys.stderr.write('Set environment variable NUM_OF_ITERATIONS or default value will be used.\n'
                     'Which is 3, by the way\n')

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)


async def util():
    available_commands = await bot.get_my_commands()

    if not available_commands:
        await bot.set_my_commands([BotCommand('start', 'greeting'),
                                   BotCommand('help', 'get list of available commands'),
                                   BotCommand('nst', 'imply neural style transfer'),
                                   BotCommand('stop', 'this command stops the nst')])


async def send_welcome(message: Message):
    await util()
    await message.reply('Hi!\nThis bot can be used for applying \nsome styles to your pics')


async def send_commands(message: Message):
    await message.reply(
                        'Use command /nst to transfer style from one pic to another\n'
                        'Use command /stop to stop style transfer\n'
                        'NOTE: this command will work until you didn\'t send both: content and style photos\n'
                        'And if you already have sent both, then nothing can stop me ;)'
    )


async def set_nst_styling(message: Message):
    await ImageStyling.get_content.set()
    await message.reply('Send me, please, two photos.\nIn first message content, in second style.\nBoth as a photo')


async def save_file(name, file_id):
    file = await bot.get_file(file_id=file_id)
    expansion = str(file.file_path).split('/')[-1].split('.')[-1]
    path = f'./{name}.{expansion}'
    os.environ[f'PATH_TO_{name.split("/")[-1].upper()}'] = path
    await file.download(path)


async def save_content(message: Message):
    file_id = message.photo[-1].file_id
    await save_file('./data/content', file_id)
    await ImageStyling.get_style.set()
    await message.reply('Now send a style')


async def save_style(message: Message, state: FSMContext):
    file_id = message.photo[-1].file_id
    await save_file('./data/style', file_id)
    await state.finish()
    await message.reply('Thanks!\nNow I\'m imposing style on the photo')
    nst_model = NST(512)
    output_image = nst_model.run_style_transfer()
    await message.answer_photo(output_image)


async def stop(message: Message, state: FSMContext):
    await state.finish()
    await message.reply('The work has been stopped')


def setup(dp_: Dispatcher):
    dp_.register_message_handler(send_welcome, CommandStart())
    dp_.register_message_handler(send_commands, CommandHelp())
    dp_.register_message_handler(set_nst_styling, commands=['nst'])
    dp_.register_message_handler(save_content, state=ImageStyling.get_content, content_types=['photo'])
    dp_.register_message_handler(save_style, state=ImageStyling.get_style, content_types=['photo'])
    dp_.register_message_handler(stop, state=ImageStyling.get_content, commands=['stop'])
    dp_.register_message_handler(stop, state=ImageStyling.get_style, commands=['stop'])


setup(dp)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)

